---
####################### Banner #########################
banner:
  title : "Let us help you solve your communications and development questions - with openness"
  image : "images/banner-art.svg"
  content : "We know everything about using openness as a means to success."
  button:
    enable : true
    label : "Contact Us"
    link : "contact"

##################### Feature ##########################
  feature:
  enable : true
  title : "Let us help you solve your communications and development questions <br> - with openness"
  feature_item:
    # feature item loop
    - name : "Open source"
      icon : "fas fa-code"
      content : "Open source code helps you get better quality, commitment and security around your code."
      
    # feature item loop
    - name : "Solution-oriented"
      icon : "fas fa-object-group"
      content : "We have experience of working in complex projects."
      
    # feature item loop
    - name : "24h Service"
      icon : "fas fa-user-clock"
      content : "We offer 24/7 support for the solutions we offer."
      
    # feature item loop
    - name : "Value For Money"
      icon : "fas fa-heart"
      content : "Whatever you need, we will help you get more value for money. We can guarantee that."
      
    # feature item loop
    - name : "Learn faster"
      icon : "fas fa-tachometer-alt"
      content : "With our solutions, you increase the pace and understanding of your industry."
      
    # feature item loop
    - name : "Cloud Support"
      icon : "fas fa-cloud"
      content : "Get information wherever you are."
      


######################### Service #####################
  service:
  enable : true
  service_item:
    # service item loop
    - title : "It is the most advanced digital marketing and it company."
      images:
      - "images/service-1.png"
      - "images/service-2.png"
      - "images/service-3.png"
      content : "Digital Openers develops solutions in information management, security and open source."
      button:
        enable : true
        label : "Contact us"
        link : "contact"
        
    # service item loop
    #- title : "It is a privately owned Information and cyber security company"
      images:
      - "images/service-1.png"
      content : "Digital Openers develops solutions in information management, security and open source."
      button:
        enable : true
        label : "Check it out"
        link : "#"
        
    # service item loop
    #- title : "It’s a team of experienced and skilled people with distributions"
      images:
      - "images/service-2.png"
      content : "Digital Openers develops solutions in information management, security and open source."
      button:
        enable : true
        label : "Check it out"
        link : "#"
        
    # service item loop
    #- title : "A company standing different from others"
      images:
      - "images/service-3.png"
      content : "Digital Openers develops solutions in information management, security and open source."
      button:
        enable : true
        label : "Check it out"
        link : "#"
        
################### Screenshot ########################
screenshot:
  enable : false
  title : "Experience the best <br> workflow with us"
  image : "images/screenshot.svg"

  

##################### Call to action #####################
call_to_action:
  enable : true
  title : "Ready to get started?"
  image : "images/cta.svg"
  content : "Let's talk! Get in touch to learn more about Openers."
  button:
    enable : true
    label : "Contact Us"
    link : "contact"
---
