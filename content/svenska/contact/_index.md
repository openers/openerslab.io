---
title: "Kontakta oss"
subtitle: "Vi är öppna för din kontakt."
# meta description
description: "Kontakta Openers. Vi är öppna för din kontakt."
draft: false
---

### Varför du kan tjäna på att kontakta oss

Vi bygger samhällsnytta. Offentlig infrastruktur. Allmännyttiga tjänster. Offentliga  tjänster. Offentliga organisationer. Processer och lösningar som når ut till alla och  alla dina användare med fantastiska upplevelser. Tänk på det som 
digitala vägar, järnvägar, tunnlar och körfält. Vi hjälper dig att bygga digitala 
vägar och fordon där dina intressenter, användare och kunder får en 
upplevelse full av lärande och utveckling samtidigt som den är glädjefylld. Alla 
betrodda av allmänheten.

Vi har många års erfarenhet av att arbeta med öppen källkod, öppna arbetsmetoder och öppen kommunikation. Ta kontakt med oss så berättar vi mer för dig.

* **Mail: [info@openers.se](mailto:info@openers.se)**
* **Address: Sankt Eriksgatan 121 B, 113 43 Stockholm, Sweden**