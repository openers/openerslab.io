---
title: "Privacy & Policy"
subtitle: "GDPR policy"
# meta description
description: "Digital Openers AB Privacy policy"
draft: false
---

## **1\. Syfte och Tillämpning**

Denna policy syftar till att säkerställa att alla personuppgifter som behandlas av vårt företag hanteras i enlighet med tillämpliga lagar och förordningar, inklusive EU:s allmänna dataskyddsförordning (GDPR). Policyn gäller för alla volontärer, anställda och andra personer som agerar på företagets vägnar.

## **2\. Dataskyddsansvarig**

Styrelsen är dataskyddsansvarig för att övervaka efterlevnaden av GDPR inom företaget och för att fungera som kontakt för frågor rörande dataskydd. Kontakta [info@openers.se](mailto:info@openers.se) vid behov.

## **3\. Insamling av Personuppgifter**

företaget ska endast samla in personuppgifter som är relevanta och nödvändiga för att uppfylla dess ändamål och verksamhet. Det är företagets ansvar att informera berörda personer om syftet med insamlingen och hur deras uppgifter kommer att användas.

## **4\. Rättslig Grund för Behandling**

företaget behandlar personuppgifter enligt flera rättsliga grunder. Dessa kan vara samtycke från de registrerade, uppfyllande av avtal eller rättsliga förpliktelser.

## **5\. Minimera Insamling och Lagring**

företaget ska sträva efter att minimera insamlingen och lagringen av personuppgifter så mycket som möjligt. Endast de uppgifter som är absolut nödvändiga för att uppnå företagets ändamål bör samlas in och lagras.

## **6\. Säkerhet och Konfidentialitet**

företaget ska vidta lämpliga tekniska och organisatoriska åtgärder för att säkerställa att personuppgifter skyddas mot oavsiktlig förlust, förstörelse eller skada samt obehörig åtkomst eller behandling.

## **7\. Delning av Personuppgifter**

Personuppgifter får endast delas med tredje part om det finns en laglig grund för sådan delning enligt GDPR eller om den registrerade har gett sitt uttryckliga samtycke. Vid delning av personuppgifter med tredje part ska lämpliga avtal och säkerhetsåtgärder träffas för att skydda uppgifterna.

## **8\. Rättigheter för Den Registrerade**

Enligt dataskyddsförordningen GDPR har du som registrerad ett antal rättigheter. Dessa inkluderar rätten till information, tillgång, rättelse, radering, begränsning av behandling, dataportabilitet och invändningar. Kontakta info@openers.se vid behov.

## **9\. Utbildning och Medvetenhet**

Styrelsen och utsedda ansvariga personer inom företagets verksamhet ska känna till företagets dataskyddspolicy för att öka medvetenheten och säkerställa efterlevnad.

## **10\. Genomförande och Uppföljning**

Styrelsen är ansvarig för att genomföra och upprätthålla denna policy. Denna policy ses regelbundet över och uppdateras vid behov.

Denna policy träder i kraft omedelbart och ska tillämpas på alla personuppgifter som behandlas av organisationen.
