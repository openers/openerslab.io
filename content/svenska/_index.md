---
####################### Banner #########################
banner:
  title : "Digital suveränitet i offentlig sektor <br> - med öppen programvara"
  image : "images/banner-art.svg"
  content : "Vi kan allt om att använda öppen programvara och öppenhet som en del av det strategiska och operativa arbete för att nå riktig digitalisering - för nationell digital suveränitet, säkerhet och framgång."
  button:
    enable : true
    label : "Kontakta oss"
    link : "contact"

##################### Feature ##########################
feature:
  enable : true
  title : "Varför Openers"
  feature_item:
    # feature item loop
    - name : "Bättre kod"
      icon : "fas fa-code"
      content : "Vi hjälper dig få bättre kvalitet, engagemang och säkerhet kring dina digitala processer - med öppen källkod."
      
    # feature item loop
    - name : "Lösningsorienterade"
      icon : "fas fa-object-group"
      content : "Vi har lång erfarenhet av att jobba i komplexa projekt."
      
    # feature item loop
    - name : "24/7 support"
      icon : "fas fa-user-clock"
      content : "Vi erbjuder 24/7 support för de lösningar och tjänster vi erbjuder."
      
    # feature item loop
    - name : "Värde för pengarna"
      icon : "fas fa-heart"
      content : "Oavsett vad ni behöver så kommer vi hjälpa er får mer värde för pengarna."
      
    # feature item loop
    - name : "Snabbare svar"
      icon : "fas fa-tachometer-alt"
      content : "Med våra lösningar ökar ni er kvalitet, takt och förståelse i er bransch eller sektor."
      
    # feature item loop
    - name : "Molntjänster"
      icon : "fas fa-cloud"
      content : "Vi rådger dig om digitalisering för molnet (cloud) från tredjeparter eller egen drift i er egen IT."
      


######################### Service #####################
service:
  enable : true
  service_item:
    # service item loop
    - title : "Openers utvecklar lösningar och strategier inom information, kommunikation och öppen källkod."
      images:
      - "images/service-1.png"
      - "images/service-2.png"
      - "images/service-3.png"
      content : "Oavsett vilket behov ni har så hjälper vi er lösa era strategiska, informatiska och kommunikations- och utvecklingsfrågor - med öppenhet."
      button:
        enable : true
        label : "Kontakta oss"
        link : "contact"
        
    # service item loop
    - title : "Vi är ett privatägt företag som hjälper kunder med research, strategi och kommunikation."
      images:
      - "images/service-1.png"
      content : "Vi hjälper er med kraften att använda öppenhet från öppen källkod och offentlighetsprincipen - oavsett om ni är användare eller utgivare av information."
      button:
        enable : true
        label : "Kontakta oss"
        link : "contact"
        
    # service item loop
    - title : "Vi är en grupp erfarna och kunniga personer som är experter på öppen källkod."
      images:
      - "images/service-2.png"
      content : "Vi hjälper er välja digitala lösningar som baseras på öppen källkod och öppen programvara - oavsett om det sköts i molnet eller i er egen IT."
      button:
        enable : true
        label : "Kolla in det"
        link : "#"
        
    # service item loop
    - title : "Ett värdeskapande löfte"
      images:
      - "images/service-3.png"
      content : "Vi levererar alltid trovärdiga lösningar som möjliggör interoperabilitet, förtroende och priseffektiva integrationer."
      button:
        enable : true
        label : "Kontakta oss"
        link : "contact"
        
################### Screenshot ########################
screenshot:
  enable : false
  title : "Upplev ett enkelt arbetsflöde med Openers"
  image : "images/screenshot.svg"

  

##################### Call to action #####################
call_to_action:
  enable : true
  title : "Redo att komma igång?"
  image : "images/cta.svg"
  content : "Låt oss prata! Hör av dig för att lära dig mer om Openers."
  button:
    enable : true
    label : "Kontakta oss"
    link : "contact"
---