---
title: "Priser"
subtitle: "Våra priser för grundläggande tjänster för support och rådgivning"
# meta description
description: "Openers erbjuder grundläggande paket för support och rådgivning inom digitalisering."
draft: false

basic:
  name : "Grundläggande plan"
  price: "12 000 SEK"
  price_per : "år"
  info : "Bäst för enskilda professionella"
  services:
  - "Svar inom 2 dagar"
  - "Customs Clearance"
  - "Time-Critical Services"
  button:
    enable : true
    label : "Get started for free"
    link : "#"
    
professional:
  name : "Professional Plan"
  price: "24 000 SEK"
  price_per : "år"
  info : "Bäst för små och medelstora företag, kommuner och regioner"
  services:
  - "Svar inom 1 dag"
  - "Customs Clearance"
  - "Time-Critical Services"
  - "Cloud Service"
  - "Best Dashboard"
  button:
    enable : true
    label : "Get started for free"
    link : "#"
    
business:
  name : "Enteprise"
  price: "48 000 SEK"
  price_per : "år"
  info : "Bäst för större företag, kommuner, regioner och myndigheter"
  services:
  - "Telefon-support samma dag"
  - "Customs Clearance"
  - "Time-Critical Services"
  button:
    enable : true
    label : "Kom igång gratis"
    link : "#"

call_to_action:
  enable : true
  title : "Behöver du hjälp med en större plan eller anpassat paket för din framgång?"
  image : "images/cta.svg"
  content : "Openers erbjuder "
  button:
    enable : true
    label : "Kontakta oss"
    link : "contact"
---
